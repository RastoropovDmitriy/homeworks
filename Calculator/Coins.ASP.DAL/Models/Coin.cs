﻿using System.Collections.Generic;

namespace Coins.ASP.DAL.Models
{
    public class Coin
    {
        public int CoinID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int PhotoID { get; set; }
        public int UserID { get; set; }

        public virtual List<Photo> Photos { get; set; }
        public virtual List<Comment> Comments { get; set; }
        public virtual User User { get; set; }

    }
}
