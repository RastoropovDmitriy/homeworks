﻿using System;
using System.Collections.Generic;

namespace Coins.ASP.DAL.Models
{
    public class User
    {
        public int UserID { get; set; }
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string HashSum { get; set; }
        public DateTime DateRegister { get; set; }

        public virtual List<Comment> Comments { get; set; }
        public virtual List<Coin> Coins { get; set; }
    }
}
