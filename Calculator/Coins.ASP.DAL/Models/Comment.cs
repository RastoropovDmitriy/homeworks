﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Coins.ASP.DAL.Models
{
    public class Comment
    {
        public int CommentID { get; set; }
        public int CoinID { get; set; }
        public int UserID { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }

        [ForeignKey("UserID")]
        public virtual User User { get; set; }

        [ForeignKey("CoinID")]
        public virtual Coin Coin { get; set; }
    }
}
