﻿namespace Coins.ASP.DAL.Models
{
    public class Photo
    {
        public int PhotoID { get; set; }
        public string PhotoDataBase64 { get; set; }
        public int CoinID { get; set; }

        public virtual Coin Coin { get; set; }
    }
}
