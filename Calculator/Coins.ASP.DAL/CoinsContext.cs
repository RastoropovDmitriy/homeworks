﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Coins.ASP.DAL.Models;

namespace Coins.ASP.DAL
{
    public class CoinsContext : DbContext
    {
        public CoinsContext() : base("CoinsContext")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Coin> Coins { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}
