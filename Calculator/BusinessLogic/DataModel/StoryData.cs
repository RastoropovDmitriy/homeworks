﻿using System;

namespace BusinessLogic.DataModel
{
    [Serializable]
    public class StoryData
    {
        public int Id { get; set; }
        public string Expression { get; set; }
        public DateTime Date { get; set; }

    }
}
