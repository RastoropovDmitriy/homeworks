﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using BusinessLogic.DataModel;

namespace BusinessLogic.Serialize
{
    public abstract class Serialize
    {
        private const string StoryPath = @"..\..\story.bin";

        protected void GoSerialize(IList<StoryData> logs)
        {
            using (var fs = new FileStream(StoryPath, FileMode.Create))
            {
                var bf = new BinaryFormatter();
                bf.Serialize(fs, logs);
            }
        }

        protected IList<StoryData> GoDeserialize()
        {
            List<StoryData> logs = new List<StoryData>();
            using (var fs = new FileStream(StoryPath, FileMode.OpenOrCreate))
            {
                var bf = new BinaryFormatter();
                if (fs.Length == 0) return logs;
                logs = (List<StoryData>)bf.Deserialize(fs);
            }
            return logs;
        } 
    }
}
