﻿
namespace BusinessLogic.Interfaces
{
    public interface ICalcExpressions
    {
        double Plus(double arg1, double arg2);
        double Minus(double arg1, double arg2);
        double Multiply(double arg1, double arg2);
        double Divided(double arg1, double arg2);
    }
}
