﻿using System.Collections.Generic;

namespace BusinessLogic.Interfaces
{
    public interface ILogRepository<T>
    {
        IList<T> SelectAll();
        void AddLog(T data);
    }
}
