﻿
namespace Calculator.Singleton.WPF.ViewModel
{
    public class CalculatorViewModel : BaseViewModel
    {
        private string _expression;
        public KeyboardViewModel Keyboard { get; set; }

        public string Expression
        {
            get { return _expression; }
            set
            {
                _expression = value;
                OnPropertyChanged("Expression");
            }
        }

        public CalculatorViewModel()
        {
            Keyboard = new KeyboardViewModel();

            Keyboard.OnKeyClick += PrintExpression;
        }

        private void PrintExpression(string expression)
        {
            Expression = expression;
        }
    }
}
