﻿using System;
using Calculator.Singleton.DAL;
using Calculator.Singleton.WPF.Command;
using System.Collections.Generic;
using System.Windows.Input;

namespace Calculator.Singleton.WPF.ViewModel
{
    public class KeyboardViewModel : BaseViewModel
    {
        private CalculatorSingleton _calculator;

        public List<List<string>> KeyList { get; set; }
        public string Expression { get; set; }
        public ICommand KeyPressCommand { get; set; }


        public delegate void Keyboard(string expression);

        public event Keyboard OnKeyClick;

        public KeyboardViewModel()
        {
            _calculator = CalculatorSingleton.GetInstance();

            GetValue();
            KeyPressCommand = new RelayCommand(KeyPressAction);
            ((RelayCommand) KeyPressCommand).IsEnabled = true;
        }

        private void KeyPressAction(object obj)
        {
            var str = obj as string;

            switch (str)
            {
                case "=":
                    Expression = _calculator.CountExpression(Expression);
                    break;
                case "←":
                    Expression = Expression.Substring(0, Expression.Length - 1);
                    break;
                case "C":
                    Expression = "";
                    break;
                default: 
                    Expression += str;
                    break;
            }

            var handler = OnKeyClick;
            if (handler != null)
                handler.Invoke(Expression);

        }

        private void GetValue()
        {
            KeyList = new List<List<string>>()
            {
                new List<string>{"7", "8", "9", "+", "←", "C"},
                new List<string>{"4", "5", "6", "-", "(", null},
                new List<string>{"1", "2", "3", "*", ")", null},
                new List<string>{"0", ",", "=", "/", "^", null}
            };
        }

    }
}
