﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
//using CalculatorApp.Classes;
//using CalcImplementation.Service;
using Calculator.Singleton.DAL;
using Calculator.BL.DataModels;
using Coins.ASP.DAL;
using Coins.ASP.DAL.Models;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            //Test();

            var i = TestDb();
            Console.WriteLine(i ? "Ok" : "Fail");
        }

        private static bool TestDb()
        {
            var context = new CoinsContext();

            context.Users.Add(new User()
            {
                Login = "Login",
                FirstName = "FName",
                LastName = "LName",
                DateRegister = DateTime.Now,
                HashSum = "arghhshgsgsrgsrjkgh132162t24h664171"
            });
            return context.SaveChanges() > 0;
        }


        private static void Test()
        {

            CalculatorSingleton _calc = CalculatorSingleton.GetInstance();
            CalculatorSingleton test = CalculatorSingleton.GetInstance();

            bool testSingle = _calc == test;

            var expression = "1+(2+5)+10*(5-2)";

            var result = _calc.CountExpression(expression);

            bool addLog = _calc.InsertLog(new LogDataModel() 
            { 
                LogId = _calc.GetLogs().Count == 0 ? 1 : _calc.GetLogs().Count +1,
                Expression = expression,
                Result = result,
                DateCount = DateTime.Now.ToString("yyyy MMMM dd")
            });

            var logs = _calc.GetLogs().ToArray();
        }

    }
}
