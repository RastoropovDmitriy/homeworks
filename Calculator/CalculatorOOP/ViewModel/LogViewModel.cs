﻿using CalcImplementation.Service;
using Calculator.Command;
using Calculator.ViewModels;
using DataLayer.DataModels;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace CalculatorOOP.ViewModel
{
    class LogViewModel : BaseViewModel
    {

        private CalculatorService _calc;
        private ObservableCollection<LogItem> _logCollection;
        public ObservableCollection<LogItem> LogCollection
        {
            get { return _logCollection; }
            set
            {
                _logCollection = value;
                OnPropertyChanged("LogCollection");   
            }
        }
        public ICommand Refresh { get; set; }


        public LogViewModel()
        {
            _calc = CalculatorService.GetInstance();
            LogCollection = new ObservableCollection<LogItem>();
            UpdateLogs();
            Refresh = new RelayCommand(RefreshCommand);
            ((RelayCommand)Refresh).IsEnabled = true;

        }

        public void UpdateLogs()
        {
            LogCollection.Clear();
            var logs = _calc.GetAllLogs().OrderByDescending(p => p.Id);

            foreach (var log in logs)
            {
                LogCollection.Add(new LogItem(log));
            }
        }

        public void RefreshCommand(object param)
        {
            UpdateLogs();
        }

        public class LogItem
        {
            public int Id { get; set; }
            public string Expression { get; set; }
            public string Result { get; set; }
            public DateTime Date { get; set; }

            public LogItem(LogDataModel log)
            {
                Id = log.Id;
                Expression = "Expression: "+log.Expression;
                Result = "Result: "+log.Result;
                Date = log.Date;
            }
        }
    }
}
