﻿using CalcImplementation.Service;
using Calculator.Command;
using Calculator.ViewModels;
using DataLayer.DataModels;
using System;
using System.Windows.Input;

namespace CalculatorOOP.ViewModel
{
    class CalculatorViewModel : BaseViewModel
    {

        #region VARIABLES

        private CalculatorService _calc;

        private string _expression;
        public string Expression
        {
            get { return _expression; }
            set
            {
                _expression = value;
                OnPropertyChanged("Expression");
            }
        }

        private string _result;
        public string Result
        {
            get { return _result; }
            set
            {
                _result = value;
                OnPropertyChanged("Result");
            }
        }

        // commands
        public ICommand Clear { get; set; }
        public ICommand CountUp { get; set; }

        #endregion


        public CalculatorViewModel()
        {
            _calc = CalculatorService.GetInstance();
            Clear = new RelayCommand(ClearCommand);
            CountUp = new RelayCommand(CountCommand);
            ((RelayCommand) Clear).IsEnabled = true;
            ((RelayCommand) CountUp).IsEnabled = true;
        }


        public void CountCommand(object param)
        {
            var input = param.ToString();
            Result = Expression;
            Result += " = " + _calc.Calculate(input);
            _calc.InsertLog(new LogDataModel()
            {
                Id = _calc.GetAllLogs().Count + 1,
                Expression = Expression,
                Result = Result,
                Date = DateTime.Now
            });
        }


        public void ClearCommand(object param)
        {
            Result = "";
            Expression = "";
        }
        
    }
}
