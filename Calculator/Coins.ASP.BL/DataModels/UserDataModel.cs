﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coins.ASP.BL.DataModels
{
    class UserDataModel
    {
        public int UserID { get; set; }
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string HashSum { get; set; }
        public DateTime DateRegister { get; set; }
    }
}
