﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Repositories
{
    public interface IRepository<T>
    {
        IList<T> SelectAll();
        void Insert(T model);
    }
}
