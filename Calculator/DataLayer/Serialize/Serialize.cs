﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace DataLayer.Serialize
{
    public abstract class Serialize<T>
    {
        protected void GoSerialize(IList<T> model, string path)
        {
            using (var fs = new FileStream(path, FileMode.Create))
            {
                var xs = new XmlSerializer(typeof(List<T>));
                xs.Serialize(fs, model);
            }
        }

        protected IList<T> GoDeserialize(string path)
        {
            using (var fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                var xs = new XmlSerializer(typeof(List<T>));
                return fs.Length > 0 ? (IList<T>)xs.Deserialize(fs) : null;
            }
        } 
    }
}
