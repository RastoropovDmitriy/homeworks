﻿using System;

namespace DataLayer.DataModels
{
    [Serializable]
    public class LogDataModel
    {
        public int Id { get; set; }
        public string Expression { get; set; }
        public string Result { get; set; }
        public DateTime Date { get; set; }
    }
}
