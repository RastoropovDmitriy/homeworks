﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.BL.DataModels
{
    public class UserDataModel
    {
        public int UserID { get; set; }
        public string Login { get; set; }
        public string HashSum { get; set; }

        public IEnumerable<LogDataModel> Logs { get; set; }
    }
}
