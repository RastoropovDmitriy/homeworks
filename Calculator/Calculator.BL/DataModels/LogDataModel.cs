﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.BL.DataModels
{
    public class LogDataModel
    {
        public int LogId { get; set; }
        public string Expression { get; set; }
        public string Result { get; set; }
        public string DateCount { get; set; }

        //public UserDataModel User { get; set; }
    }
}
