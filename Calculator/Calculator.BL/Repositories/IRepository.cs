﻿using System.Collections.Generic;

namespace Calculator.BL.Repositories
{
    public interface IRepository<TDataModel, T>
    {
        bool Add(TDataModel model);
        bool Update(T id);
        bool Remove(T id);
        IList<TDataModel> GetAll();


    }
}
