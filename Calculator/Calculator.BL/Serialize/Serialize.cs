﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Calculator.BL.Serialize
{
    public abstract class Serialize<T>
    {
        protected void GoSerialize(IList<T> model, string path)
        {
            using (var fs = new FileStream(path, FileMode.Create))
            {
                var xs = new XmlSerializer(typeof(List<T>));
                xs.Serialize(fs, model);
            }
        }

        protected IList<T> GoDeserialize(string path)
        {
            var data = new List<T>();
            using (var fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                if (fs.Length == 0) return data;
                var xs = new XmlSerializer(typeof(List<T>));
                data = (List<T>)xs.Deserialize(fs);
            }
            return data;
        } 
    }
}
