﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.BL.ReversePolishNotation
{
    public class RPN : Checking
    {
        public RPN()
        {
            //var operators = new[] { "sin", "cos", "tan", "sqrt", "ln" };
        }

        public string GetExpression(string input)
        {
            var output = string.Empty;
            var BinOper = new Stack<char>();
            // TODO sinle operators
            var SingleOper = new Stack<string>(); 

            for (var i = 0; i < input.Length; i++)
            {
                if (IsDelimeter(input[i])) continue;

                if (Char.IsDigit(input[i]))
                {
                    while (!IsDelimeter(input[i]) && !IsBinaryOperator(input[i]))
                    {
                        output += input[i];
                        i++;
                        if (i == input.Length) break;
                    }
                    output += " "; 
                    i--;
                }

                if (IsBinaryOperator(input[i]))
                {
                    switch (input[i])
                    {
                        case '(':
                            BinOper.Push(input[i]);
                            break;
                        case ')':
                        {
                            var s = BinOper.Pop();
                            while (s != '(')
                            {
                                output += s.ToString() + ' ';
                                s = BinOper.Pop();
                            }
                        }
                            break;
                        default:
                            if (BinOper.Count > 0)
                                if (GetPriority(input[i]) <= GetPriority(BinOper.Peek()))
                                    output += BinOper.Pop() + " "; 
                            BinOper.Push(char.Parse(input[i].ToString())); 
                            break;
                    }
                }
            }
            while (BinOper.Count > 0)
                output += BinOper.Pop() + " ";
            return output;
        }


        public string Counting(string input)
        {
            double result = 0;
            var temp = new Stack<double>();

            for (var i = 0; i < input.Length; i++) 
            {
                //Если символ - цифра, то читаем все число и записываем на вершину стека
                if (Char.IsDigit(input[i]))
                {
                    string a = string.Empty;

                    while (!IsDelimeter(input[i]) && !IsBinaryOperator(input[i])) //Пока не разделитель
                    {
                        a += input[i]; //Добавляем
                        i++;
                        if (i == input.Length) break;
                    }
                    temp.Push(double.Parse(a));
                    i--;
                }
                else if (IsBinaryOperator(input[i]))
                {
                    //Берем два последних значения из стека
                    double a = temp.Pop();
                    double b = temp.Pop();

                    switch (input[i])
                    {
                        case '+': result = b + a; break;
                        case '-': result = b - a; break;
                        case '*': result = b * a; break;
                        case '/': result = b / a; break;
                        case '^': result = double.Parse(Math.Pow(double.Parse(b.ToString()), double.Parse(a.ToString())).ToString()); break;
                    }
                    temp.Push(result); 
                }
            }
            return temp.Peek().ToString();
        }

    }
}
