﻿using System.Linq;

namespace Calculator.BL.ReversePolishNotation
{
    public abstract class Checking
    {
        protected static bool IsDelimeter(char c)
        {
            return " =".IndexOf(c) != -1;
        }

        protected static bool IsBinaryOperator(char c)
        {
            return "+-*/^()".IndexOf(c) != -1;
        }

        //TODO
        protected static bool IsSingleOperator(string c, string[] operatorArr)
        {
            return operatorArr.Select(p => p == c).Any();
        }

        protected static byte GetPriority(char c)
        {
            switch (c)
            {
                case '(': return 0;
                case ')': return 1;
                case '+': return 2;
                case '-': return 3;
                case '*': return 4;
                case '/': return 4;
                case '^': return 5;
                default: return 6;
            }
        }
    }
}
