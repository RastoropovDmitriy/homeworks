﻿using Calculator.BL.DataModels;
using Calculator.BL.Repositories;
using Calculator.BL.Serialize;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Singleton.DAL.Repositories
{
    public class LogRepository : Serialize<LogDataModel>, IRepository<LogDataModel, int>
    {
        private const string Path = @"log.tmp";

        public bool Add(LogDataModel model)
        {
            var data = GoDeserialize(Path);
            data.Add(model);
            try
            {
                GoSerialize(data, Path);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public IList<LogDataModel> GetAll()
        {
            return GoDeserialize(Path);
        }

        #region No use methods
        public bool Update(int id)
        {
            return false;
        }

        public bool Update(string id)
        {
            return false;
        }

        public bool Remove(int id)
        {
            return false;
        }

        public bool Remove(string id)
        {
            return false;
        }
        #endregion



    }
}
