﻿using Calculator.BL.DataModels;
using Calculator.BL.ReversePolishNotation;
using Calculator.Singleton.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Singleton.DAL
{
    public class CalculatorSingleton
    {
        private static CalculatorSingleton instance;

        private readonly LogRepository _logs;
        private readonly RPN _rpn;

        protected CalculatorSingleton()
        {
            _logs = new LogRepository();
            _rpn = new RPN();
        }

        public static CalculatorSingleton GetInstance()
        {
            instance = instance ?? new CalculatorSingleton();
            return instance;
        }

        public string CountExpression(string input)
        {
            string expression;
            string result;
            try
            {
                expression = _rpn.GetExpression(input);
                result = _rpn.Counting(expression);
                InsertLog(new LogDataModel()
                {
                    LogId = GetLogs().Count == 0 ? 1 : GetLogs().Count + 1,
                    Expression = input,
                    Result = result,
                    DateCount = DateTime.Now.ToString("yyyy MMMM dd")
                });
            }
            catch (Exception)
            {
                result = "error";
            }
            return result;
        }

        public IList<LogDataModel> GetLogs()
        {
            return _logs.GetAll();
        }

        public bool InsertLog(LogDataModel model)
        {
            return _logs.Add(model);
        }

    }
}
