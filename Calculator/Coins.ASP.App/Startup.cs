﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Coins.ASP.App.Startup))]
namespace Coins.ASP.App
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
