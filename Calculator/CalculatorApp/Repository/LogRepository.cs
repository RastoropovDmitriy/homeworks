﻿using BusinessLogic.DataModel;
using BusinessLogic.Interfaces;
using BusinessLogic.Serialize;
using System.Collections.Generic;

namespace CalculatorApp.Repository
{
    class LogRepository : Serialize, ILogRepository<StoryData>
    {
        public IList<StoryData> SelectAll()
        {
            return GoDeserialize();
        }

        public void AddLog(StoryData data)
        {
            var currentData = GoDeserialize();
            currentData.Add(data);
            GoSerialize(currentData);
        }
    }
}
