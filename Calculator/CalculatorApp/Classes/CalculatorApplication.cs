﻿using System.Collections.Generic;
using BusinessLogic.DataModel;
using CalculatorApp.Repository;

namespace CalculatorApp.Classes
{
    public class CalculatorApplication : Actions
    {
        private static CalculatorApplication instance;

        private readonly LogRepository _logs;

        protected CalculatorApplication()
        {
            _logs = new LogRepository();
        }

        public static CalculatorApplication Instance()
        {
            instance = instance ?? new CalculatorApplication();
            return instance;
        }

        public void CountUp(out double rezult, double arg1 = 0, double arg2 = 0, string sign = " ")
        {
            switch (sign)
            {
                case "+":
                    rezult = Plus(arg1, arg1);
                    break;
                case "-":
                    rezult = Minus(arg1, arg2);
                    break;
                case "*":
                    rezult = Multiply(arg1, arg2);
                    break;
                case "/":
                    rezult = Divided(arg1, arg2);
                    break;
                default:
                    rezult = arg1;
                    break;
            }
        }

        public IList<StoryData> SelectAllLogs()
        {
            return _logs.SelectAll();
        } 

        public void SaveLog(StoryData data)
        {
            _logs.AddLog(data);
        }
    }
}
