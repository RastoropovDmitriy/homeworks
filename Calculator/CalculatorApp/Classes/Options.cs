﻿using System.Collections.Generic;
using BusinessLogic.DataModel;
using CalculatorApp.Repository;

namespace CalculatorApp.Classes
{
    class Options
    {
        private readonly LogRepository _logs;

        public Options()
        {
            _logs = new LogRepository();

        }

        public IList<StoryData> GetLogs()
        {
            return _logs.SelectAll();
        } 


    }
}
