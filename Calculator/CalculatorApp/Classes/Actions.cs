﻿using BusinessLogic.Interfaces;

namespace CalculatorApp.Classes
{
    public abstract class Actions
    {

        public double Plus(double arg1, double arg2)
        {
            return arg1 + arg2;
        }

        public double Minus(double arg1, double arg2)
        {
            return arg1 - arg2;
        }

        public double Multiply(double arg1, double arg2)
        {
            return arg1*arg2;
        }

        public double Divided(double arg1, double arg2)
        {
            return arg1/arg2;
        }
    }
}
