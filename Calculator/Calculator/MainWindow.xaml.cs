﻿using Calculator.ViewModels;
using System.Windows;

namespace Calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            
            InitializeComponent();
            var CalcView = new CalculatorViewModel();
            var LogView = new LogViewModel();
            CalculatorView1.DataContext = CalcView;
            LogView1.DataContext = LogView;
        }
    }
}
