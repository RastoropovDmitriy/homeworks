﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using BusinessLogic.DataModel;
using Calculator.Command;
using CalculatorApp.Classes;

namespace Calculator.ViewModels
{
    class CalculatorViewModel : BaseViewModel
    {
        #region Variable
        private CalculatorApplication _calcApp;
        public ICommand Number { get; set; }
        public ICommand Clear { get; set; }
        public ICommand Sign { get; set; }
        private string _expression;
        public string Expression
        {
            get { return _expression; }
            set
            {
                _expression = value;
                OnPropertyChanged("Expression");
            }
        }

        private string _template;
        public string Template
        {
            get { return _template; }
            set
            {
                _template = value;
                OnPropertyChanged("Template");
            }
        }
        #endregion

        public CalculatorViewModel()
        {
            _calcApp = CalculatorApplication.Instance();
            Number = new RelayCommand(ClickNumber);
            Clear = new RelayCommand(ClearCommand);
            Sign = new RelayCommand(ClickSign);
            ((RelayCommand) Number).IsEnabled = true;
            ((RelayCommand) Sign).IsEnabled = true;
            ((RelayCommand) Clear).IsEnabled = true;
        }
        
        private void ClickNumber(object param)
        {
            var str = param.ToString();
            Template += str;
            Expression += str;
        }

        private void ClickSign(object param)
        {
            var str = param.ToString();
            Template = "";
            Expression += str;
            if (str.Equals(" = "))
                Count();
        }

        private void Count()
        {
            var tmpArr = Expression.Split(' ');
            double res;
            if(!tmpArr.Any()) return;
            _calcApp.CountUp(out res, double.Parse(tmpArr[0]), double.Parse(tmpArr[2]), tmpArr[1]);
            Expression += res;
            _calcApp.SaveLog(new StoryData()
            {
                Date = DateTime.Now,
                Expression = Expression,
                Id = _calcApp.SelectAllLogs().Count+1,
            });
            Template = "";
        }

        private void ClearCommand(object param)
        {
            Template = "";
            Expression = "";
        }

    }
}
