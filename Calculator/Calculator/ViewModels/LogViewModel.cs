﻿using System.Linq;
using BusinessLogic.DataModel;
using CalculatorApp.Classes;
using System;
using System.Collections.ObjectModel;

namespace Calculator.ViewModels
{
    class LogViewModel : BaseViewModel
    {
        private CalculatorApplication _calcApp;

        private ObservableCollection<LogItem> _calcLogCollection;
        public ObservableCollection<LogItem> CalcLogCollection
        {
            get { return _calcLogCollection; }
            set
            {
                _calcLogCollection = value;
                OnPropertyChanged("CalcLogCollection");
            }
        }

        public LogViewModel()
        {
            _calcApp = CalculatorApplication.Instance();
            CalcLogCollection = new ObservableCollection<LogItem>();
            UpdateLogs();
        }

        public void UpdateLogs()
        {
            CalcLogCollection.Clear();
            var logs = _calcApp.SelectAllLogs().OrderByDescending(p=> p.Id);

            foreach (var log in logs)
            {
                CalcLogCollection.Add(new LogItem(log));
            }
        }

        public class LogItem
        {
            public int Id { get; set; }
            public string Expression { get; set; }
            public DateTime Date { get; set; }

            public LogItem(StoryData log)
            {
                Id = log.Id;
                Expression = log.Expression;
                Date = log.Date;
            }
        }
    }
}
