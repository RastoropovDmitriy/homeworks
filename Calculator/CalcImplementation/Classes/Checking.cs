﻿
namespace CalcImplementation.Classes
{
    public abstract class Checking
    {

        protected static bool IsDelimeter(char c)
        {
            return " =".IndexOf(c) != -1;
        }

        protected static bool IsOperator(char c)
        {
            return "+-*/^()".IndexOf(c) != -1;
        }

        protected static byte GetPriority(char c)
        {
            switch (c)
            {
                case '(': return 0;
                case ')': return 1;
                case '+': return 2;
                case '-': return 3;
                case '*': return 4;
                case '/': return 4;
                case '^': return 5;
                default: return 6;
            }
        }
    }
}
