﻿using DataLayer.DataModels;
using DataLayer.Repositories;
using DataLayer.Serialize;
using System.Collections.Generic;

namespace CalcImplementation.Repositories
{
    class LogRepository : Serialize<LogDataModel>, IRepository<LogDataModel>
    {
        private const string Path = @"..\..\logs.xml";

        public IList<LogDataModel> SelectAll()
        {
            return  GoDeserialize(Path);
        }

        public void Insert(LogDataModel model)
        {
            var data = GoDeserialize(Path) ?? new List<LogDataModel>();
            data.Add(model);
            GoSerialize(data, Path);
        }
    }
}
