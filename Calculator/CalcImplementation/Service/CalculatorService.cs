﻿using CalcImplementation.Classes;
using CalcImplementation.Repositories;
using DataLayer.DataModels;
using System;
using System.Collections.Generic;

namespace CalcImplementation.Service
{
    public class CalculatorService : Checking
    {

        public delegate void LogUpdate();

        public event LogUpdate OnLogUpdate;


        private LogRepository _logs;
        private static CalculatorService instance;

        protected CalculatorService()
        {
            _logs = new LogRepository();
        }



        public static CalculatorService GetInstance()
        {
            instance = instance ?? new CalculatorService();
            return instance;
        }



        private void OnLogUpdateAction()
        {
            if (OnLogUpdate != null)
                OnLogUpdate();
        }

        public string Calculate(string input)
        {
            var output = GetExpression(input);
            string result;
            try
            {
                result = Counting(output);
            }
            catch (Exception)
            {
                result = "error";
            }
            return result;
        }



        private string GetExpression(string input)
        {
            var output = string.Empty; //Строка для хранения выражения
            var operStack = new Stack<char>(); //Стек для хранения операторов

            for (int i = 0; i < input.Length; i++) 
            {
                if (IsDelimeter(input[i]))
                    continue; 
                //Если символ - цифра, то считываем все число
                if (Char.IsDigit(input[i])) //Если цифра
                {
                    //Читаем до разделителя или оператора, что бы получить число
                    while (!IsDelimeter(input[i]) && !IsOperator(input[i]))
                    {
                        output += input[i]; 
                        i++; 
                        if (i == input.Length) break; //Если символ - последний, то выходим из цикла
                    }
                    output += " "; //Дописываем после числа пробел в строку с выражением
                    i--;
                }
                //Если символ - оператор
                if (IsOperator(input[i]))
                {
                    if (input[i] == '(') //Если символ - открывающая скобка
                        operStack.Push(input[i]); //Записываем её в стек
                    else if (input[i] == ')') //Если символ - закрывающая скобка
                    {
                        //Выписываем все операторы до открывающей скобки в строку
                        char s = operStack.Pop();
                        while (s != '(')
                        {
                            output += s.ToString() + ' ';
                            s = operStack.Pop();
                        }
                    }
                    else
                    {
                        if (operStack.Count > 0) //Если в стеке есть элементы
                            if (GetPriority(input[i]) <= GetPriority(operStack.Peek())) //И если приоритет нашего оператора меньше или равен приоритету оператора на вершине стека
                                output += operStack.Pop().ToString() + " "; //То добавляем последний оператор из стека в строку с выражением

                        operStack.Push(char.Parse(input[i].ToString())); //Если стек пуст, или же приоритет оператора выше - добавляем операторов на вершину стека
                    }
                }
            }

            while (operStack.Count > 0)
                output += operStack.Pop() + " ";
            return output;
        }

        private string Counting(string input)
        {
            double result = 0;
            var temp = new Stack<double>();

            for (var i = 0; i < input.Length; i++) //Для каждого символа в строке
            {
                //Если символ - цифра, то читаем все число и записываем на вершину стека
                if (Char.IsDigit(input[i]))
                {
                    string a = string.Empty;

                    while (!IsDelimeter(input[i]) && !IsOperator(input[i])) //Пока не разделитель
                    {
                        a += input[i]; //Добавляем
                        i++;
                        if (i == input.Length) break;
                    }
                    temp.Push(double.Parse(a));
                    i--;
                }
                else if (IsOperator(input[i])) //Если символ - оператор
                {
                    //Берем два последних значения из стека
                    double a = temp.Pop();
                    double b = temp.Pop();

                    switch (input[i]) //И производим над ними действие, согласно оператору
                    {
                        case '+': result = b + a; break;
                        case '-': result = b - a; break;
                        case '*': result = b * a; break;
                        case '/': result = b / a; break;
                        case '^': result = double.Parse(Math.Pow(double.Parse(b.ToString()), double.Parse(a.ToString())).ToString()); break;
                    }
                    temp.Push(result); //Результат вычисления записываем обратно в стек
                }
            }
            return temp.Peek().ToString(); //Забираем результат всех вычислений из стека и возвращаем его
        }

        public IList<LogDataModel> GetAllLogs()
        {
            return _logs.SelectAll() ?? new List<LogDataModel>();
        }

        public void InsertLog(LogDataModel data)
        {
            _logs.Insert(data);
        }
    }
}
